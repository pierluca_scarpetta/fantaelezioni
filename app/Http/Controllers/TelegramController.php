<?php

namespace App\Http\Controllers;
// use Exception;

use Carbon\Carbon;
use Telegram\Bot\Api;
use App\Models\Telegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramController extends Controller
{
    protected $telegram;
    protected $chat_id;
    protected $username;
    protected $text;
    protected $name;
    protected $surname;
    protected $button;
    protected $message_id;
    
    
    public function __construct()
    {
        $this->telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
    }
    
    public function getMe()
    {
        $response = $this->telegram->getMe();
        return $response;
    }
    
    public function setWebHook()
    {
        $url = 'https://pierlucascarpetta.it/' . env('TELEGRAM_BOT_TOKEN') . '/webhook';
        $response = $this->telegram->setWebhook(['url' => $url]);        
        return $response == true ? dd('ok') : dd($response);
    }
    
    public function handleRequest(Request $request)
    {
        log::error($request);

        // area bottone
        if(isset($request->callback_query['data'])){
            $this->chat_id = $request->callback_query['message']['chat']['id'];
            $this->username = $request->callback_query['message']['from']['username'];
            $this->text = $request->callback_query['data'];
            if(isset($request->callback_query['message']['from']['first_name'])){
                $this->name = $request->callback_query['message']['from']['first_name'];
            }
            if( isset($request->callback_query['message']['from']['last_name'])){
                $this->surname = $request->callback_query['message']['from']['last_name'];
            }
            if(isset($request->callback_query['message']['message_id'])){
                $this->message_id = $request->callback_query['message']['message_id'];
            }
        // area non bottone
        }else{
            $this->chat_id = $request['message']['chat']['id'];
            $this->username = $request['message']['from']['username'];
            if(isset($request['message']['text'])){
                $this->text = $request['message']['text'];
            }
            if(isset($request['message']['message_id'])){
                $this->message_id = $request['message']['message_id'];
            }
            if(isset($request['message']['from']['first_name'])){
                $this->name = $request['message']['from']['first_name'];
            }
            if( isset($request['message']['from']['last_name'])){
                $this->surname = $request['message']['from']['last_name'];
            }
        }

            log::error($request);
            log::error('chat_id:'.$this->chat_id);
            log::error('username:'.$this->username);
            log::error('text:'.$this->text);
            log::error('name:'.$this->name);
            log::error('surname:'.$this->surname);
            log::error('surname:'.$this->message_id);

            switch ($this->text) {
                case '/start':
                    $this->messaggioBenvenuto($this->username);
                break;
                case 'prenotazione':
                    $this->delete();
                    $this->prenotazione();
                break; 
                case 'altre_richieste':
                    $this->delete();
                    $this->altreRichieste();
                break;
                case 'servizio_1':
                    $this->registrazioneServizio('servizio_1');
                break;
                case 'data_1':
                case 'data_2':
                case 'data_3':
                case 'data_4':
                case 'data_5':
                case 'data_6':
                case 'data_7':
                    $this->delete();
                    $this->confermaData($this->text);
                break;
                case 'back_date':
                    $this->delete();
                    $this->prenotazione();
                default:
                $this->delete();
                $this->erroreDigitalizzazione();
                break;
            }
        }
        
    public function messaggioBenvenuto($username){
        $messaggio = 'Benvenuto '.$username.PHP_EOL.'Cosa possiamo fare per te?';
        $this->menuPrincipale($messaggio);
    }

    public function confermaData($data){
        $data = explode("_",$data);
        $giorno = $data[1];
        $giorno = Carbon::now()->addDays($giorno)->format("d / m / Y");
        $messaggio = "Sei sicuro di confermare l'appuntamento per il giorno ".$giorno;
        $keyboard = Keyboard::make()
        ->inline()
        ->row(Keyboard::inlineButton(['text' => "Si, confermo", 'callback_data' => 'prenotazione_'.$giorno, 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Torna indietro", 'callback_data' => 'back_date', 'switch_inline_query' => true]));
        $this->sendMessageKeyboard($messaggio,$keyboard);
    }

    public function menuPrincipale($messaggio){
        $keyboard = Keyboard::make()
        ->inline()
        ->row(Keyboard::inlineButton(['text' => "Prenota un'appuntamento", 'callback_data' => 'prenotazione', 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Altre richieste", 'callback_data' => 'altre_richieste', 'switch_inline_query' => true]));
        $this->sendMessageKeyboard($messaggio,$keyboard);
    }

    public function listaServizi(){
        $messaggio = 'Cosa vuoi prenotare?';
        $keyboard = Keyboard::make()
        ->inline()
        ->row(Keyboard::inlineButton(['text' => "Servizio 1", 'callback_data' => 'servizio_1', 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Servizio 2", 'callback_data' => 'servizio_2', 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Servizio 3", 'callback_data' => 'servizio_3', 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Servizio 4", 'callback_data' => 'servizio_4', 'switch_inline_query' => true]))
        ->row(Keyboard::inlineButton(['text' => "Servizio 5", 'callback_data' => 'servizio_5', 'switch_inline_query' => true]));
        $this->sendMessageKeyboard($messaggio,$keyboard);
    }
    public function registrazioneServizio($tipoServizio){
        Telegram::insert(['idUser' => $this->chat_id,'username' => $this->username,'tipo servizio'=>$tipoServizio]);
        $this->dayPrenotazione();
    }

    public function dayPrenotazione(){
        $messaggio = 'Quando vuoi prenotare2?';
        $keyboard = Keyboard::make()->inline();
        $i = 0;
        $giorno = 1;
        for ($i=1; $i <= 7; $i++) { 
            # code...
            $newDateTime = Carbon::now()->addDays($i);

            if($data = $newDateTime->isWeekday()){
                $data = null;
                $data = $newDateTime->format('l');
                $mese = $newDateTime->format('F');
                $giorno = $newDateTime->format('d');

                
                switch ($mese) {
                    case 'January':
                        $mese = 'Gennaio';
                    break;
                    case 'February':
                        $mese = "Febbraio";
                    break;
                    case 'March':
                        $mese = "Marzo";
                    break;
                    case 'April':
                        $mese = "Aprile";
                    break;
                    case 'May':
                        $mese = "Maggio";
                    break;
                    case 'June':
                        $mese = "Giugno";
                    break;
                    case 'July':
                        $mese = "Luglio";
                    break;
                    case 'August':
                        $mese = "Agosto";
                    break;
                    case 'September':
                        $mese = "Settembre";
                    break;
                    case 'October':
                        $mese = "Ottobre";
                    break;
                    case 'November':
                        $mese = "Novembre";
                    break;
                    case 'December':
                        $mese = "Dicembre";
                    break;
                }

                switch ($data) {
                    case 'Monday':
                    $stamp = 'Lunedì '.$giorno.' '.$mese;
                    break;
                    case 'Tuesday':
                    $stamp = 'Martedì '.$giorno.', '.$mese;
                    break;
                    case 'Wednesday':
                    $stamp = 'Mercoledì '.$giorno.', '.$mese;
                    break;
                    case 'Thursday':
                    $stamp = 'Giovedì '.$giorno.', '.$mese;
                    break;
                    case 'Friday':
                    $stamp = 'Venerdì '.$giorno.', '.$mese;
                    break;  
                }
                $keyboard->row(Keyboard::inlineButton(['text' => $stamp, 'callback_data' => 'data_'.$i, 'switch_inline_query' => true]));
            }
        }
        $this->sendMessageKeyboard($messaggio,$keyboard);
    }
    public function prenotazione(){
        // $this->sendMessageKeyboard($messaggio,$keyboard);
        $this->listaServizi();
    }

    public function altreRichieste(){
        $messaggio = 'Altri servizi';
        $this->sendMessage($messaggio);
    }

    public function erroreDigitalizzazione(){
        $messaggio = 'Mi dispiace '.$this->username.' ma non ho capito.\nTi posso consigliare di: ';
        $this->menuPrincipale($messaggio);
    }
    

                                    
    protected function sendMessage($message, $parse_html = false)
    {
        $data = [
            'chat_id' => $this->chat_id,
            'text' => $message,
        ];
        
        if ($parse_html) $data['parse_mode'] = 'HTML';
        
        $this->telegram->sendMessage($data);
    }

    protected function sendMessageKeyboard($message, $button, $parse_html = false)
    {
        $data = [
            'chat_id' => $this->chat_id,
            'text' => $message,
            'reply_markup' => $button
        ];
        log::error($data);
        
        if ($parse_html) $data['parse_mode'] = 'HTML';
        
        $this->telegram->sendMessage($data);
    }

    protected function delete()
    {
        $this->telegram->deleteMessage(['chat_id' => $this->chat_id, 'message_id' => $this->message_id]);
    }

   
}
                                